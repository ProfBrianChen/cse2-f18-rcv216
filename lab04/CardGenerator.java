   //Program by Ryan Vignogna, for CSE 02 Lab 04 on 9/21/18
   //
   //Generates a random card from a deck of 52. 
   //This is done by generating a random number between 1-52
   //Each number between 1-52 is assigned a specific suite and face value
   //
public class CardGenerator{
  
  public static void main(String args[]){
    
    //define card range for random number generator
    int max = 52;
    int min = 1;
    int range = max - min + 1;
    
    //generate random number
    int cardNumber = (int) (Math.random() * range) + min;
   
    //to test random number generation
    //System.out.println(cardNumber);
    
    //variables
    //1-13 represent diamonds
    //14-26 represent clubs
    //27-39 represent hearts
    //40-52 represent spades
    //card identity then goes in ascending order from ace to king
    //eg 16 is 3 of clubs, 17 is 4 of clubs, 38 is queen of hearts
    
    String cardIdentity = ""; //card value, aka 2-10, jack, queen, king, ace
    String cardSuit = ""; //card suit
    
    //to assign suit based on random number generated
    switch (cardNumber) {
      case 1: case 2: case 3: case 4: 
      case 5: case 6: case 7: case 8: 
      case 9: case 10: case 11:  case 12: 
      case 13:
        cardSuit = "Diamonds";
        break;
        
      case 14:  case 15: case 16: case 17:
      case 18:  case 19:  case 20:  case 21:
      case 22:  case 23:  case 24:  case 25:
      case 26:
        cardSuit = "Clubs";
        break;
        
      case 27:  case 28:  case 29:  case 30:
      case 31:  case 32:  case 33:  case 34:
      case 35:  case 36:  case 37:  case 38:
      case 39:
        cardSuit = "Hearts";
				break;
        
      case 40:  case 41:  case 42:  case 43:  
      case 44:  case 45:  case 46:  case 47:
      case 48:  case 49:  case 50:  case 51:
      case 52:
        cardSuit = "Spades";
        break;
		}
    
    //to test suit generation
		//System.out.println(cardSuit);
    
    //find card identity based on random number mod 13, eg if card number is 33 then 33 mod 13 is is 7
    int cardMod = cardNumber % 13;
    
    if (cardMod == 1){
      cardIdentity = "Ace";
    }
    
     if (cardMod == 2){
      cardIdentity = "2";
    }
    
   if (cardMod == 3){
      cardIdentity = "3";
    }
    
     if (cardMod == 4){
      cardIdentity = "4";
    }
    
        if (cardMod == 5){
      cardIdentity = "5";
    }
    
     if (cardMod == 6){
      cardIdentity = "6";
    }
    
   if (cardMod == 7){
      cardIdentity = "7";
    }
    
     if (cardMod == 8){
      cardIdentity = "8";
    }

        if (cardMod == 9){
      cardIdentity = "9";
    }
    
     if (cardMod == 10){
      cardIdentity = "10";
    }
    
   if (cardMod == 11){
      cardIdentity = "Jack";
    }
    
     if (cardMod == 12){
      cardIdentity = "Queen";
    }
    
    if (cardMod == 0){
      cardIdentity = "King";
    }
    
    //to test identity output
    //System.out.println(cardIdentity);
    
    //print random cardIdentity
    System.out.println("You picked the " + cardIdentity + " of " + cardSuit);
    
    
  }
  
}
    
    