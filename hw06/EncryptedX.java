//Program by Ryan Vignogna for Lab06 on 21 October 2018
//
//Program to make a hidden X in a n x n grid, where n is given by the user
//
//
//
import java.util.Scanner;
import java.text.*;

public class EncryptedX{
  public static void main (String [] args){
    
    Scanner sc = new Scanner (System.in);
    
    //input variables
    int usrChoice = 0;
    
    //ask user for integer 0-100, only accept those values or gives error message
    //checks for integer input, and then checks that the number is 0-100
    boolean usrCheck = false;   
    while (usrCheck==false){
      System.out.print("Please input an integer 0-100: "); 
      usrCheck = sc.hasNextInt();
    if (usrCheck){
      usrChoice=sc.nextInt();
      if ((usrChoice) <0 || (usrChoice >100)){
        usrCheck = false;
        System.out.println("Error!");
      }
      }
    else{
        System.out.println("Error!");
      sc.next();
      }
    }
    
    //i corresponds to row, j corresponds to 'column' or the character printed 
    int i,j;
    
    //prints the pattern    
    for (i = 1; i <= usrChoice; i++) { //determines row number
        			for (j = 1; j <= usrChoice; j++) {
        				if (j == i) { //prints space when column and row number are equal, eg first column of first row is always a space
        					System.out.print(" ");
        			} 
                else if (j == (usrChoice - i)) { //prints space to match the first space, eg if usrChoice=10, this prints a space at 10-0 for the first row, or 10
        					System.out.print(" ");
        				} 
                else {
        					System.out.print("*");//prints a star everywhere else
        				}
        			}
        			System.out.println();//return new line
        		}
        	
    
  }
}