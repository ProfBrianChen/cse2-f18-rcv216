//Program for Lab09 by Ryan Vignogna on 16 November 2018
//
//Prints a revered array, the reverse of that array, then the reverse of that array
//
//
//
import java.util.*;

public class lab09{

public static int[] copy(int[] intArray){
  
  //input variables
  int inputLength = intArray.length;
  int[] copyArray = new int [inputLength];
  
  //copies array passed into method to a new array output by this method
  for(int i = 0; i < inputLength; i++){
    copyArray[i] = intArray[i];
  }
  
  return copyArray;
  
}
  
public static void inverter(int [] intArray){
  
  //input variables
  int inputLength = intArray.length;
  
  //inverts input array into new array
  for (int i = 0; i < inputLength/2; i++){
    int temp = intArray[i];
    intArray[i] = intArray[inputLength - i - 1];
    intArray[inputLength - i - 1] = temp;
  }
  
  return;
}
  
public static int[] inverter2(int [] intArray2){
  
  int[] intArray = copy(intArray2);
  
  int inputLength = intArray.length;
  
  //inverts input array into new array
  for (int i = 0; i < inputLength/2; i++){
    int temp = intArray[i];
    intArray[i] = intArray[inputLength - i - 1];
    intArray[inputLength - i - 1] = temp;
  }
  
  return intArray;
  
}
  
public static void print(int [] intArray){
  
  int inputLength = intArray.length;
  for(int i = 0; i<inputLength; i++){
    System.out.print(intArray[i]+ " ");
  }
  System.out.println("");
  return;
}
  
public static void main(String args[]){
  
  //makes copies of array0
  int[] array0 = {0,1,2,3,4,5,6,7,8,9};
  int[] array1 = copy(array0);
  int[] array2 = copy(array0);
  int [] array3;
  
  //assigned tasks
  inverter(array0);
  print(array0);
  inverter2(array1);
  print(array1);
  array3 = inverter2(array2);
  print(array3);

 }
}