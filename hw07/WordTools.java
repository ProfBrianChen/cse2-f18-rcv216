//Program by Ryan Vignogna for CSE HW07 on 28 October 2018
//
//User inputs a String of their choice
//Program asks user for a menu choice
//Program then perfoms several tasks: counts non-WS, counts words, finds a specified text, replace !s, or shorten words
//
//
import java.util.Scanner;
import java.util.Random;

public class WordTools{
  
  public static String sampleText(){
    
    Scanner scan = new Scanner (System.in);
    
    
    System.out.println("Please enter a string of your choosing:");
    String userString = scan.nextLine();
    System.out.println("You entered: ");
    
    return userString;
  } //prompts user for text and returns as a String
  
  public static char printMenu(){
    
   Scanner scan = new Scanner (System.in);
    
    char menuChoice = 'z';
    boolean menuTF = false;
    
     //prints main menu
     System.out.println("Menu:");
     System.out.println("[c] - find number of non-whitespace characters");
     System.out.println("[w] - find number of words");
     System.out.println("[f] - find text");
     System.out.println("[r] - replace all !'s");
     System.out.println("[s] - shorten spaces");
     System.out.println("[q] - quit");
    
    //ensures a correct character is enetered
    while (menuTF == false){
    menuChoice = scan.next().charAt(0);
      if (menuChoice == 'q'){
        menuChoice = 'q';
        menuTF = true;
        }
      else if (menuChoice == 'c'){
        menuChoice = 'c';
        menuTF = true;
       }
      else if (menuChoice == 'w'){
        menuChoice = 'w';
        menuTF = true;
       }
      else if (menuChoice == 'f'){
         menuChoice = 'f';
        menuTF = true;
       }
      else if (menuChoice == 'r'){
        menuChoice = 'r';
        menuTF = true;
       }
      else if (menuChoice == 's'){
        menuChoice = 's';
        menuTF = true;
        }
      else{
        System.out.println("Please enter a valid character.");
      }
    }
      return menuChoice;
      
    
      
      
      
      
      
      
      
      
    
    
  } //displays menu and prompts user to choose an option, returns choice as a char
  
  public static int getNumOfNonWSCharacters(String textFromMain){
    
    String text = textFromMain; //string from user
    int importantChars = 0; //stores counts of non-WS characters
    
    for (int i = 0; i < text.length(); i++){ //loop to go through entire string
      
      if (text.charAt(i) != ' '){ //increments counter when a non-WS is encountered
        importantChars++;
      }
    }
    return importantChars;  //returns integer value 
  } //counts number of non-whitespaces in user text
  
  public static int getNumOfWords(String textFromMain){
    
    String text = textFromMain; //string from user
    int numWords = 1; //stores counts of words
    
        for (int i = 0; i <= text.length()-1; i++){ //loop to go through entire string
            if (text.charAt(i) == ' ' && text.charAt(i+1) != ' ') //increments numWords if a chracter is a whitespace followed by a non-whitespace
            {
                numWords++;
            }
        }

    return numWords;
  } //counts number of words in user text
  
  public static String findText(String textFromMain, String wordOIFromMain){
    //note: I did this before learning about Index, so sorry for the mess...but it still works
    String text = textFromMain; //string from user
    String wordOI = wordOIFromMain; //word of interest from main method
    int numWordsOfInterest = 0; //stores counts of specified word
    int textLength = text.length(); //length of text
    int wOILength = wordOI.length(); //length of word of interest
      
      for (int i = 0; i < text.length(); i++){ //loop to go through entire string
            if (wordOI.charAt(0) == text.charAt(i)){ //triggers next loop if the first letter of WOI is encountered
              if (i>0){
                if(text.charAt(i-1) == ' '){ //checks for space in front of character
                  if(i + wOILength < textLength){ 
                    if(text.charAt(i + wOILength) != ' '){ //makes sure there's a space after the word
                      continue;}
                  }
                }
              else{
                continue;
               }
              }
        int counter = 1; //checks word length, then each letter
        for (int j = 1; j < wOILength; j++){
          i++;
          if(wordOI.charAt(j) != text.charAt(i)){ //if letters don't match, the loop ends
            break;
          }
          else{
            counter++;
          }
        }
        if (counter == wOILength){
          numWordsOfInterest++; //counts to word total
        }
       }
      }
    
    String findTextString = (wordOI + " instances: " + numWordsOfInterest); //prints this sentence with word of interest and its count
    return findTextString; //return the sentence above as a string
  } //counts the number of times a word (or phrase) of interest is in user text
  
  public static String replaceExclamation(String textFromMain){ 
    
    String text = textFromMain; //text from main method
    String editText = ""; //edited text to be returned
    
    editText = text.replace('!', '.'); //replaces ! with .

    return editText; //outputs text as a String
  } //replaces '!' with '.' in user text
  
  public static String shortenSpace(String textFromMain){
    
    String text = textFromMain;
    String editText = ""; //edited text to be returned
    
    editText = text.replace("  ", " "); //replaces double space with single space

    return editText; //outputs text as a String
  } //replaces double spaces with single spaces
    
  
  public static void main (String[] args){ //main method
    
    Scanner scan = new Scanner (System.in);
    String text = sampleText(); //stores text entered in sampleText method
    char menu = printMenu(); //stores user choice from printMenu method
    String wordOIFromMain = "";
    
    while(menu != 'q'){ //loops through menu until quit (q) is typed
    
     //see corresponding methods for details 
    if (menu == 'c'){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
      System.out.println(" ");
      menu = printMenu();
    }
    
    if (menu == 'w'){
      System.out.println("Number of words: " + getNumOfWords(text));
      System.out.println(" ");
      menu = printMenu();
    }
    
    if (menu == 'f'){
      System.out.println("Enter word or phrase to be found: ");
      wordOIFromMain = scan.next();
      System.out.println(findText(text,wordOIFromMain));
      System.out.println(" ");
      menu = printMenu();
    }
   
    if (menu == 'r'){
      System.out.println("Edited text: " + replaceExclamation(text));
      System.out.println(" ");
      menu = printMenu();
    }
    
    if (menu == 's'){
      System.out.println("Edited text: " + shortenSpace(text));
      System.out.println(" ");
      menu = printMenu();
    }
   }
    
  }  
}