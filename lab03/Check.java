//Program by Ryan Vignogna 9/14/18 for CSE 02, Lab 03
import java.util.Scanner;
// Program obtains cost of a check and the precentage tip to be paid, and how the check is being split
//The program outputs how much each person then must pay
//
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {

         Scanner myScanner = new Scanner (System.in);
          
          //Ask the user for the cost of the check, store that number
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
          double checkCost = myScanner.nextDouble ();
          
          //Ask the user for the precent tip they want to give, store that value
          System.out.print("Enter the precentage tip that you wish to pay as a whole number (in the form xx): ");
          double tipPercent = myScanner.nextDouble();
          tipPercent /= 100; //we want the precentage to be a decimal value
          
          //Ask the user for the number of people splitting the check, store that value
          System.out.print("Enter the number of people who went out to dinner: ");
          int numPeople = myScanner.nextInt();
          
          //variables
          double totalCost, //total cost of the meal
                 costPerPerson; //total cost per person
          int dollars, //cost in whole dollars
              dimes, pennies; //for storing digits, to the right of the decimal point
          
          //calculations
          totalCost = checkCost * (1 + tipPercent); //get the whole amount, drop the fraction
          costPerPerson = totalCost / numPeople; //get cost per person
          dollars = (int) costPerPerson; //get whole dollar amount per person
          dimes = (int) (costPerPerson * 10) % 10; //get dimes amount, mod operator returns remainder
          pennies = (int) (costPerPerson *100) % 10; //get pennies...again, mod returns remainder
          
          //print final results, aka how much each person owes based on cost, tip, and number of people
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
          
          
}  //end of main method   
  	} //end of class