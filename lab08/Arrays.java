//Program for Lab08 by Ryan Vignogna on 9 November 2018
//
//Generates an array of random numbers then prints how many times each number was generated
//
//
//
import java.util.*;

public class Arrays{

  public static void main(String args[]){
    
    //variables
    int randArray [] = new int [100];
    int countArray [] = new int [100];
    Random r = new Random();
    int randNum = 0;
    int count = 0;
    
    //stores array with random values
    System.out.println("Array 1 holds the following integers: ");
    for (int i = 0; i <= 99; i++){
      randArray[i] = r.nextInt(99);
      System.out.print(randArray[i] + " ");
    }
    System.out.println("");
    
    //counts number of number's occurence in randArray, stores those in countArray
    for (int j = 0; j <= 99; j++){
     for (int k = 0; k <= 99; k++){
       if (randArray[k] == j){
         count++;}
       else {continue;}
       }
     countArray[j] = count;
      count = 0;
     }      
      
    //prints occurences of numbers in randArray 
    for (int f = 0; f <= 99; f++){  
    if (countArray[f] != 0){
    System.out.println(f + " occurs " + countArray[f] + " times");
    }
    }
    
    
  }
}
      
    
    