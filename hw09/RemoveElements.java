//Program for Lab09 by Ryan Vignogna on 22 November 2018
//
//Generates a random array, deletes an index based on user input, removes indicies containing a number enetered by user
//
//
//
import java.util.*;

public class RemoveElements{
  
public static String listArray(int num[]){
	
  //prints array with curly braces
  String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

public static int[] randomInput(){
  
  //variables
  Random randy = new Random();
  int [] randomArray = new int [10];
  int arrayLength = randomArray.length;
  
  //randomly puts integers between 0-9 into array
  for (int i = 0; i < arrayLength; i++){
     int randomNum = randy.nextInt(9);
     randomArray[i] = randomNum;
  }
  
  return randomArray;
  
}
  
public static int [] delete(int [] list, int pos){
  
  //variables
  Scanner scan = new Scanner(System.in);
  int inputArrayLength = list.length;
  int shortLength = inputArrayLength-1;
  int[] shortArray = new int[shortLength];
  int counter = 0;
  
  while((pos>inputArrayLength) || (pos<0)){
    System.out.println("Error! Outside of array length. Please enter a new index:");
    pos = scan.nextInt();
  }
    
  //fills new array with values until it hits position 'pos', skips that position, then fills the rest
   for(int j = 0; j < shortLength; j++){
    if(j == pos){
      counter++; //when the 'pos' to be deleted is reached, the counter is incremented instead of filling the shortArray
    }
    else{
    shortArray[j-counter] = list[j]; //if the pos to be deleted is reached, the counter will allign the two arrays
    }
  }
  
  return shortArray;
  
}
  
public static int[] remove(int[] list, int target){
  
  //variables
  int inputArrayLength = list.length;
  int counter = 0;
  
  //determine how often a number appears to determine size of new array
  for(int i = 0; i < inputArrayLength; i++){
    if(list[i] == target){
      counter++;
    }
  }

  //variables
  int arrayLength = (inputArrayLength-counter);
  int[] newArray = new int[arrayLength];
  int counter2 = 0;
  
  //copies array to new array, excluding the target number by making use of the previously described counter trick
  for(int j = 0; j < inputArrayLength; j++){
    if(list[j] == target){
      counter2++;
    }
    else{
    newArray[j-counter2] = list[j];
    }
  }
  
  return newArray;
  
}
  
public static void main (String args[]){  
  
  	Scanner scan = new Scanner(System.in);
  
    //variables
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer = "";
  
  
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
    //user specifies a value (entered as an index) they want removed from an array
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    //user specifies a number they want removed from an array, and all instences of that number is removed by the remove method
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
    //prompts the user if they want to repeat the program
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));

 }
}