//Program for Lab09 by Ryan Vignogna on 22 November 2018
//
//Takes user-entered grades, performs binary search and then scrambles and performs linear search
//
//
//
import java.util.*;

public class CSE2Linear{
  
  public static boolean check(int[] userArray, int arrayLength){
    
    //checks if userArray is in ascending order and prints error message if it isn't
    for (int i = 0; i < arrayLength-1; i++) {
      if (userArray[i] > userArray[i+1]) {
      System.out.println("Error! Please enter integers in ascending order");
      return false;
          }
      }
    return true;
    
    }
  
  public static int binary(int[] userArray, int arrayLength, int searchVal){
 
      int mid;
      int low;
      int high;
      int counter = 1;
      
      low = 0;
      high = arrayLength - 1;

      while (high >= low) {
         mid = (high + low) / 2;
         if (userArray[mid] < searchVal) {
            low = mid + 1;
           counter++;
         } 
         else if (userArray[mid] > searchVal) {
            high = mid - 1;
           counter++;
         } 
         else {
            return counter;
         }
      }

      return -1; // not found
   }
  
  public static int[] scramble(int[] userArray, int arrayLength){
    
    Random randy = new Random();
    
    //randomly shuffles the userArray
    for (int i = arrayLength - 1; i > 0; i--){
      int index = randy.nextInt(i + 1);
      int a = userArray[index];
      userArray[index] = userArray[i];
      userArray[i] = a;
    }
    
    return userArray;
    
  }
  
  public static int linear(int[] userArray, int arrayLength, int searchVal){
      
    int counter = 0;
    
      for (int i = 0; i < arrayLength; ++i){
        counter++;
         if (userArray[i] == searchVal){
            return counter;
         }
      }
      
      return -1;
  }
  
  public static void main(String args[]){

    //input variables
    Scanner sc = new Scanner(System.in);
    int[] userArray = new int [15];
    int arrayLength = userArray.length;
    boolean ascendCheck = false;
    
    System.out.println("Please enter ascending ints for CSE2 final grades:");
    
    //checks that integers are entered, that they are between 0-100, and that they are in ascending order
    while(ascendCheck == false){
    for(int i = 0; i < arrayLength; i++){
    boolean intCheck = false;
    while ((intCheck == false) || (userArray[i]>100) || (userArray[i]<0) ){
      if ((userArray[i]>100) || (userArray[i]<0)){
      System.out.println("Error! Please enter grade as an integer between 0-100");
      }
      System.out.print("Enter student " + (i+1) + "'s grade: "); 
      intCheck = sc.hasNextInt();
    if (intCheck){
      userArray[i]=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter grade as an integer value");
      sc.next();
      }
     }
    }
      ascendCheck = check(userArray, arrayLength); //puts array into ascendCheck to ensure its ascending order, will prompt entries again if its not
    }
    
    //prints out the integers the user entered
    System.out.println("You entered:");
    for (int j = 0; j < arrayLength; j++){
      System.out.print(userArray[j] + " ");
    }
        
    //initiates binary search for specified value
    int low = userArray[0];
    int high = userArray[14];
    System.out.println(" ");
    System.out.print("Enter a grade to search for: ");
    int searchVal = sc.nextInt();
    int result = binary(userArray, arrayLength, searchVal);
    
    //prints if value was found and how many iterations of the binary search it took to find it
    if(result == -1){
      System.out.println("Grade not found");
    }
    else{
      System.out.println("Grade found in the list with " + result + " iteration(s)");
    }
    
    //shuffles array and performs linear search
    System.out.println("Shuffled:");
    scramble(userArray, arrayLength);
    for (int j = 0; j < arrayLength; j++){
      System.out.print(userArray[j] + " ");
     }
    System.out.println(" ");
    System.out.println("Enter a grade to search for: ");
    searchVal = sc.nextInt();
    int result2 = linear(userArray, arrayLength, searchVal);
    
    if(result2 == -1){
      System.out.println("Grade not found");
    }
    else{
      System.out.println("Grade found in the list with " + result2 + " iteration(s)");
    }
    
 }
}