   //Program by Ryan Vignogna, for CSE 02 HW 04 on 9/25/18
   //
   //Craps program made using only 'if' statements 
   //
   //Asks user if they want to randomly roll dice or input their own roll
   //Outputs Craps slang term for their dice roll
   //
import java.util.Scanner;
public class CrapsIf{
  
  public static void main(String args[]){
    
  Scanner myScanner = new Scanner (System.in);
    
    //prints welcome message, and message asking use if they want to use input values or random values
    System.out.println("Welcome to Craps Slang Terminology Generator");
    System.out.println("Would you like to input dice rolls or use random rolls? Type 'input' or 'random':");
          String userChoice = myScanner.next();
               
    
    //define range for random number generator
    int max = 6;
    int min = 1;
    int range = max - min + 1;
    
    //generate random number
    int randomDieOne = (int) (Math.random() * range) + min;
    int randomDieTwo = (int) (Math.random() * range) + min;
    
    
    //values of each dice
    int dieOne = 0;
    int dieTwo = 0;
    
    //based on user choice of input or random, different paths are taken
    //if input, the user is prompted to input dice rolls 1 then 2
    //if a value not between 1-6 is entered, they will be prompted to enter again
    //if random, numbers from random generator are used and printed to screen
    //if neither random nor input is typed, the program will end
    if (userChoice.equals("input")){
      System.out.println("Please input die roll one:");
        dieOne = myScanner.nextInt();
      
      while (dieOne > 6 || dieOne < 0){                             //makes sure value is 1-6
        System.out.println("Incorrect value, please pick a number 1-6");
        dieOne = myScanner.nextInt();
      }
      
      System.out.println("Please input die roll two:");
        dieTwo = myScanner.nextInt();
      
      while (dieTwo > 6 || dieTwo < 0){                             //makes sure value is 1-6
        System.out.println("Incorrect value, please pick a number 1-6");
        dieTwo = myScanner.nextInt();
      }
    }
    else if(userChoice.equals ("random")){
      dieOne = randomDieOne;
      dieTwo = randomDieTwo;
      System.out.println("Die roll one: " + dieOne);
      System.out.println("Die roll two: " + dieTwo);
    }
   
    else{
      System.out.println("Incorrect input, please try again");
    }
    
    
    //slang term outputs, based on diceOne and diceTwo values
    //these are printed to the screen
    if((dieOne == 1) && (dieTwo == 1)){
      System.out.println("You rolled: Snake Eyes");
    }
    
    else if((dieOne == 1) && (dieTwo == 2)){
      System.out.println("You rolled: Ace Deuce");
    }
    
     else if((dieOne == 2) && (dieTwo == 1)){
      System.out.println("You rolled: Ace Deuce");
    }
    
     else if((dieOne == 3) && (dieTwo == 1)){
      System.out.println("You rolled: Easy Four");
    }
    
     else if((dieOne == 1) && (dieTwo == 3)){
      System.out.println("You rolled: Easy Four");
    }
    
      else if((dieOne == 4) && (dieTwo == 1)){
      System.out.println("You rolled: Fever Five");
    }
    
     else if((dieOne == 1) && (dieTwo == 4)){
      System.out.println("You rolled: Fever Five");
    }
    
      else if((dieOne == 5) && (dieTwo == 1)){
      System.out.println("You rolled: Easy Six");
    }
    
     else if((dieOne == 1) && (dieTwo == 5)){
      System.out.println("You rolled: Easy Six");
    }
    
      else if((dieOne == 6) && (dieTwo == 1)){
      System.out.println("You rolled: Seven Out");
    }
    
     else if((dieOne == 1) && (dieTwo == 6)){
      System.out.println("You rolled: Seven Out");
    }
    
    else if((dieOne == 2) && (dieTwo == 2)){
      System.out.println("You rolled: Hard Four");
    }
    
     else if((dieOne == 2) && (dieTwo == 3)){
      System.out.println("You rolled: Fever Five");
    }
    
      else if((dieOne == 3) && (dieTwo == 2)){
      System.out.println("You rolled: Fever Five");
    }
    
     else if((dieOne == 4) && (dieTwo == 2)){
      System.out.println("You rolled: Easy Six");
    }
    
      else if((dieOne == 2) && (dieTwo == 4)){
      System.out.println("You rolled: Easy Six");
    }
    
    else if((dieOne == 2) && (dieTwo == 5)){
      System.out.println("You rolled: Seven Out");
    }
    
      else if((dieOne == 5) && (dieTwo == 2)){
      System.out.println("You rolled: Seven Out");
    }
    
     else if((dieOne == 6) && (dieTwo == 2)){
      System.out.println("You rolled: Easy Eight");
    }
    
      else if((dieOne == 2) && (dieTwo == 6)){
      System.out.println("You rolled: Easy Eight");
    }
    
      else if((dieOne == 3) && (dieTwo == 3)){
      System.out.println("You rolled: Hard Six");
    }
    
      else if((dieOne == 3) && (dieTwo == 4)){
      System.out.println("You rolled: Seven out");
    }
    
     else if((dieOne == 4) && (dieTwo == 3)){
      System.out.println("You rolled: Seven out");
    }
    
      else if((dieOne == 3) && (dieTwo == 5)){
      System.out.println("You rolled: Easy Eight");
    }
    
    else if((dieOne == 5) && (dieTwo == 3)){
      System.out.println("You rolled: Easy Eight");
    }
    
      else if((dieOne == 3) && (dieTwo == 6)){
      System.out.println("You rolled: Nine");
    }
    
     else if((dieOne == 6) && (dieTwo == 3)){
      System.out.println("You rolled: Nine");
    }
    
    else if((dieOne == 4) && (dieTwo == 4)){
      System.out.println("You rolled: Hard Eight");
    }
    
      else if((dieOne == 4) && (dieTwo == 5)){
      System.out.println("You rolled: Nine");
    }
    
    else if((dieOne == 5) && (dieTwo == 4)){
      System.out.println("You rolled: Nine");
    }
    
      else if((dieOne == 4) && (dieTwo == 6)){
      System.out.println("You rolled: Easy Ten");
    }
    
     else if((dieOne == 6) && (dieTwo == 4)){
      System.out.println("You rolled: Easy Ten");
    }
    
    else if((dieOne == 5) && (dieTwo == 5)){
      System.out.println("You rolled: Hard Ten");
    }
    
      else if((dieOne == 6) && (dieTwo == 5)){
      System.out.println("You rolled: Yo-leven");
    }
    
     else if((dieOne == 5) && (dieTwo == 6)){
      System.out.println("You rolled: Yo-leven");
    }
    
     else if((dieOne == 6) && (dieTwo == 6)){
      System.out.println("You rolled: Boxcars");
    }
    
    else{
      System.out.print("Error");
    }
    
    
  
    
  }
}
                     