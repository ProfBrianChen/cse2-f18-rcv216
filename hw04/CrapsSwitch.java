   //Program by Ryan Vignogna, for CSE 02 HW 04 on 9/25/18
   //
   //Craps program made using only switch statements 
   //
   //Asks user if they want to randomly roll dice or input their own roll
   //Outputs Craps slang term for their dice roll
   //
import java.util.Scanner;
public class CrapsSwitch{
  
  public static void main(String args[]){
    
  Scanner myScanner = new Scanner (System.in);
    
    //prints welcome message, and message asking use if they want to use input values or random values
    System.out.println("Welcome to Craps Slang Terminology Generator");
    System.out.println("Would you like to input dice rolls or use random rolls? Type 'input' or 'random':");
          String userChoice = myScanner.next();
               
    
    //define range for random number generator
    int max = 6;
    int min = 1;
    int range = max - min + 1;
    
    //generate random number
    int randomDieOne = (int) (Math.random() * range) + min;
    int randomDieTwo = (int) (Math.random() * range) + min;
    
    
    //values of each dice
    int dieOne = 0;
    int dieTwo = 0;
    
    //based on user choice of input or random, different paths are taken
    //if 'input', the user is prompted to input dice rolls 1 then 2
    //if a value not between 1-6 is entered, they will be prompted to enter again
    //if 'random', numbers from random generator are used and printed to screen
    //if neither random nor input is typed, the program will end
    switch (userChoice){
      case "input":
        System.out.println("Please input die roll one:");
        dieOne = myScanner.nextInt();
      
      while (dieOne > 6 || dieOne < 0){
        System.out.println("Incorrect value, please pick a number 1-6");
        dieOne = myScanner.nextInt();}
       
        System.out.println("Please input die roll two:");
        dieTwo = myScanner.nextInt();
      
      while (dieTwo > 6 || dieTwo < 0){
        System.out.println("Incorrect value, please pick a number 1-6");
        dieTwo = myScanner.nextInt();}
      break;
   
      case "random":
      dieOne = randomDieOne;
      dieTwo = randomDieTwo;
      System.out.println("Die roll one: " + dieOne);
      System.out.println("Die roll two: " + dieTwo);
      break;
   
      default:
      System.out.println("Incorrect input, please try again");
    }
    
    //converts dice values to strings for use in switch assignment
    String stringOne = ("" + dieOne);
    String stringTwo = ("" + dieTwo);
    String diceUnion = (stringOne + stringTwo);
    
    //slang term outputs, based on diceOne and diceTwo values
    //these are printed to the screen
    switch (diceUnion){
      case "11":  
      System.out.println("You rolled: Snake Eyes");
      break;
    
      case "12": case "21":
      System.out.println("You rolled: Ace Deuce");
      break;
    
      case "13": case "31":
      System.out.println("You rolled: Easy Four");
      break;
        
      case "14": case "41": case "23": case "32":
      System.out.println("You rolled: Fever Five");
      break;
   
      case "15": case "51": case "24": case "42":
      System.out.println("You rolled: Easy Six");
      break;
    
      case "16": case "61": case "25": case "52": case "34": case "43":
      System.out.println("You rolled: Seven Out");
      break;
 
      case "22":
      System.out.println("You rolled: Hard Four");
      break;
    
      case "26": case "62": case "35": case "53":
      System.out.println("You rolled: Easy Eight");
      break;

    
      case "33":
      System.out.println("You rolled: Hard Six");
      break;
    
      case "36": case "63": case "45": case "54":
      System.out.println("You rolled: Nine");
      break;
        
      case "44":
      System.out.println("You rolled: Hard Eight");
      break;
        
      case "46": case "64":  
      System.out.println("You rolled: Easy Ten");
      break;
    
      case "55":
      System.out.println("You rolled: Hard Ten");
      break;
    
   
      case "56": case "65":
      System.out.println("You rolled: Yo-leven");
      break;
    
    
      case "66":
      System.out.println("You rolled: Boxcars");
      break;
      
      default:  
      System.out.print("Error");
       
    }
    
    
    
    
    
    }
}