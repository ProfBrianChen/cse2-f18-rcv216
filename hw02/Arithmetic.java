   //Program by Ryan Vignogna, for CSE 02 HW 02 on 9/9/18
   /*
   Computes cost of items purchased, including a 6% sales tax
   */
public class Arithmetic{
  //main method for any Java program
  public static void main(String args[]){
    
  /* input variables  */
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98;
    
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99; //cost per shit
    
    int numBelts = 1; //number of belts
    double beltPrice = 33.99; //cost per beltPrice
    
    double paSalesTax = 0.06; //the PA tax rate
    
    
  /* output values */
    double totalCostPants;  //total cost of pants
    double totalCostShirts; //total cost of sweatshirts
    double totalCostBelts;  //total cost of belts
    double taxTotalPants;   //tax charged on pants
    double taxTotalShirts;  //tax charged on sweathsirts
    double taxTotalBelts;   //tax charged on belts
    double preTaxTotal;     //total before taxTotalBelt
    double taxTotalDouble;  //total tax due, intermediate double
    int taxTotalInt;        //total tax due, intermeidate integer 
    double taxTotal;        //actual tax total, to two decimal places
    double grandTotal;      //total costs plus total taxes
    
    totalCostPants = numPants * pantsPrice;   //calculates cost of pants based on number purchased
    totalCostShirts = numShirts * shirtPrice; //calculate cost of shirts based on number purchased
    totalCostBelts = numBelts * beltPrice;    //calculate cost of belts based on number purchased
    
    taxTotalPants = paSalesTax * totalCostPants;  //calculate total tax due from pants
    taxTotalShirts = paSalesTax * totalCostShirts;//calculate total tax due from shirts
    taxTotalBelts = paSalesTax * totalCostBelts;  //calculate total tax due from belts
    
    preTaxTotal = totalCostBelts + totalCostShirts + totalCostPants; //calculate total before tax applies
    taxTotalDouble = (taxTotalBelts + taxTotalShirts + taxTotalPants)*100; //calculate total taxes, double times 100 to generate integer
    taxTotalInt = ((int) taxTotalDouble ) * 1; //convert taxTotalDouble to an integer
    taxTotal = (double) taxTotalInt / 100; //convert taxTotalInt back to a double with two decimal places
   
    grandTotal = preTaxTotal + taxTotal; //calculate sum of item total plus tax of those items
    
  /* print results to screen */
    System.out.println("Cost of pants is $" + totalCostPants); //print pants total
    System.out.println("Cost of sweatshirts is $" + totalCostShirts); //print shirt total
    System.out.println("Cost of belts is $" + totalCostBelts); //print belts total
    System.out.println("Subtotal is $" + preTaxTotal); //print subtotal before taxes
    System.out.println("PA Sales Tax total is $" + taxTotal); //print tax amount
    System.out.println("Grand total is $" + grandTotal); //print subtotal plus tax total
    
  }
}
      