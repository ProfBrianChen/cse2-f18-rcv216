//Program by Ryan Vignogna for Lab06
//
//Pattern D, prints desired pyramid pattern using two 'for' loops
//
//
//
import java.util.Scanner;
import java.text.*;

public class PatternD{
  public static void main (String [] args){
    
    Scanner sc = new Scanner (System.in);
    
    //input variables
    int usrChoice = 0;
    
    //ask user for integer 1-10, only accept those values or gives error message
    //checks for integer input, and then checks that the number is 1-10
    boolean usrCheck = false;   
    while (usrCheck==false){
      System.out.print("Please input an integer 1-10: "); 
      usrCheck = sc.hasNextInt();
    if (usrCheck){
      usrChoice=sc.nextInt();
      if ((usrChoice) <1 || (usrChoice >10)){
        usrCheck = false;
        System.out.println("Error!");
      }
      }
    else{
        System.out.println("Error!");
      sc.next();
      }
    }
    int j;
 //prints pattern D, based on user input integer 
 //first for loop determines row number
 //second for loop determines the numbers
 for(int i = 0; i <=usrChoice ; i++){
 for (int k = usrChoice-i; k > 0; k--){
   System.out.print(k+" ");
   }
   System.out.println();
 }
    
    
    
    
    
    
    
   }
 }

