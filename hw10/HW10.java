//Program for HW10 by Ryan Vignogna on 3 December 2018
//
//Tic-Tac-Toe Program to be played by two users
//Prints Winner or Draw Message when game is complete
//
//
import java.util.*;

public class HW10{
  
  public static void printMatrix(char[][] board){
    
    //prints board using nested for loop
     for(int row = 0; row < board.length; row++){
      for(int column = 0; column < board[row].length; column++){
      System.out.print(board[row][column] + " ");
      }
     System.out.println(); 
    }
  }

  public static boolean okayInput(int input, char[][] board){
    
      //alligns user input (int 1-9) with corresponding matrix coordinates
      char check = '0';
    
      switch(input){
          case 1: check = board[0][0]; break;
          case 2: check = board[0][1]; break;
          case 3: check = board[0][2]; break;
          case 4: check = board[1][0]; break;
          case 5: check = board[1][1]; break;
          case 6: check = board[1][2]; break;
          case 7: check = board[2][0]; break;
          case 8: check = board[2][1]; break;
          case 9: check = board[2][2]; break;
        default: System.out.println("isFree error");
      }
   
   //ensures user input is in a blank space, if not this method returns false
   if(check == 'X'){
     return false;
   }
    else if(check == 'O'){
      return false;
    }
    else{
    return true;
    }
  }
     
  public static boolean rowWin(char[][] board){
    
      //looks if three X's or O's are in a row
      if((board[0][0] == board[0][1]) && (board[0][1] == board [0][2])){
        return true;
      }
      if((board[1][0] == board[1][1]) && (board[1][1] == board[1][2])){
        return true;
      }
      if((board[2][0] == board[2][1]) && (board[2][1] == board[2][2])){
        return true;
      }
  return false;
  }
  
  public static boolean columnWin(char[][] board){
    
        //looks if three X's or O's are in a column
       if((board[0][0] == board[1][0]) && (board[1][0] == board [2][0])){
        return true;
      }
      else if((board[0][1] == board[1][1]) && (board[1][1] == board[2][1])){
        return true;
      }
      else if((board[0][2] == board[1][2]) && (board[1][2] == board[2][2])){
        return true;
      }
  else{
    return false;
   }
  }
  
  public static boolean crossWin(char[][] board){
    
        //looks if three X's or O's are in a diagonal
       if((board[0][0] == board[1][1]) && (board[1][1] == board [2][2])){
        return true;
      }
      if((board[0][2] == board[1][1]) && (board[1][1] == board[2][0])){
        return true;
      }
  return false;
  }
      
  public static void main(String args[]){
    
    //input variables, sets up tic-tac-toe board
    Scanner sc = new Scanner(System.in);
    boolean goldStar = false;
    int input = -1;
    String playerNum = "";
    boolean rowCheck = false;
    boolean colCheck = false;
    boolean crossCheck = false;
    char [][] board = {
      {'1','2','3'},
      {'4','5','6'},
      {'7','8','9'}      
     };
    
    printMatrix(board);
    
    //this is kind of a mess but this prompts users for inputs and passes board into methods checking win conditions and input viability
    //this also checks inputs to ensure they're integers and between 1-9
    for (int i = 0; i<9; i++){
      goldStar = false; //will turn true when input is acceptable
      int j = i % 2; //player 1's turn if even turn, player 2's turn if odd turn
      if(j == 0){ //player 1
        playerNum = "Player 1"; //used in winner message
        while(goldStar == false){
        boolean intCheck = false;   
    while ((intCheck==false) || (input<1) || (input>9)){ //requires that input be an integer between 1-9
      System.out.print("Player 1: Enter O Location: "); 
      intCheck = sc.hasNextInt();  
    if (intCheck){
      input=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter an integer value");
      sc.next();
      }
    }
          goldStar = okayInput(input, board); //passes integer to method checking that it wasn't already selected by a player
        }
        //takes user input and enters their character (in this case a O token) to their selected matrix point
        if(input == 1){board[0][0] = 'O';}
        if(input == 2){board[0][1] = 'O';}
        if(input == 3){board[0][2] = 'O';}
        if(input == 4){board[1][0] = 'O';}
        if(input == 5){board[1][1] = 'O';}
        if(input == 6){board[1][2] = 'O';}
        if(input == 7){board[2][0] = 'O';}
        if(input == 8){board[2][1] = 'O';}
        if(input == 9){board[2][2] = 'O';}
      }
      
      if(j == 1){ //player 2, follows some processes as above for player 1
        playerNum = "Player 2";
        while(goldStar == false){
       boolean intCheck = false;   
    while ((intCheck==false) || (input<1) || (input>9)){
      System.out.print("Player 2: Enter X Location: "); 
      intCheck = sc.hasNextInt();  
    if (intCheck){
      input=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter an integer value");
      sc.next();
      }
    }
          goldStar = okayInput(input, board);
        }
        if(input == 1){board[0][0] = 'X';}
        if(input == 2){board[0][1] = 'X';}
        if(input == 3){board[0][2] = 'X';}
        if(input == 4){board[1][0] = 'X';}
        if(input == 5){board[1][1] = 'X';}
        if(input == 6){board[1][2] = 'X';}
        if(input == 7){board[2][0] = 'X';}
        if(input == 8){board[2][1] = 'X';}
        if(input == 9){board[2][2] = 'X';}
      }
      
      printMatrix(board); //prints board after token was added
      rowCheck = rowWin(board); //checks for 3 in a row
      colCheck = columnWin(board); //checks for 3 in a column
      crossCheck = crossWin(board); //checks for 3 in a diagonal
      
      if(rowCheck == true){
        System.out.println("Congratulations! " + playerNum + " wins!");
        break;
      }
      
      if(crossCheck == true){
        System.out.println("Congratulations! " + playerNum + " wins!");
        break;
      }
      
      if(colCheck == true){
        System.out.println("Congratulations! " + playerNum + " wins!");
        break;
      }
      
     } //end for loop
    
    //prints if no winner is found
    if ((crossCheck == false) && (rowCheck == false) && (colCheck == false)){
      System.out.println("Draw! No winner this time");
    }
    
  } //end main method
 } //end class