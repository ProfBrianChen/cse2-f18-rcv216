//Program by Ryan Vignogna for Lab07
//
//Generates random thesis sentences until user is happy with it
//Prints a random paragraph using that thesis
//
//
import java.util.Scanner;
import java.util.Random;

public class Lab07{
  
  public static String Adjective(){
    
    Random randy = new Random();
    int randomInt = randy.nextInt(10);
    
    String adjOut = "";
    
    switch(randomInt){
      case 0: adjOut = "black";
        break;
        
      case 1: adjOut = "yellow";
        break;
        
      case 2: adjOut = "brown";
        break;
        
      case 3: adjOut = "white";
        break;
        
      case 4: adjOut = "red";
        break;
        
      case 5: adjOut = "blue";
        break;
        
      case 6: adjOut = "purple";
        break;
        
      case 7: adjOut = "gold";
        break;
        
      case 8: adjOut = "green";
        break;
        
      case 9: adjOut = "pink";
        break;
        
      default: System.out.print("there is an error");
    }
    
    return adjOut;
    
  }
  
  public static String SubNoun(){
    
    Random randy = new Random();
    int randomInt = randy.nextInt(10);
    
    String subOut = "";
    
    switch(randomInt){
      case 0: subOut = "dog";
        break;
        
      case 1: subOut = "cat";
        break;
        
      case 2: subOut = "bird";
        break;
        
      case 3: subOut = "fish";
        break;
        
      case 4: subOut = "insect";
        break;
        
      case 5: subOut = "fungus";
        break;
        
      case 6: subOut = "horse";
        break;
        
      case 7: subOut = "cow";
        break;
        
      case 8: subOut = "sheep";
        break;
        
      case 9: subOut = "goat";
        break;
        
      default: System.out.print("there is an error");
    }
    
    return subOut;
    
  }
  
  public static String Verb(){
    
    Random randy = new Random();
    int randomInt = randy.nextInt(10);
    
    String verbOut = "";
    
    switch(randomInt){
      case 0: verbOut = "ate";
        break;
        
      case 1: verbOut = "smelled";
        break;
        
      case 2: verbOut = "licked";
        break;
        
      case 3: verbOut = "watched";
        break;
        
      case 4: verbOut = "ignored";
        break;
        
      case 5: verbOut = "listened for";
        break;
        
      case 6: verbOut = "passed";
        break;
        
      case 7: verbOut = "devoured";
        break;
        
      case 8: verbOut = "tasted";
        break;
        
      case 9: verbOut = "loved";
        break;
        
      default: System.out.print("there is an error");
    }
    
    return verbOut;
    
  }
  
  public static String ObjNoun(){
    
    Random randy = new Random();
    int randomInt = randy.nextInt(10);
    
    String objOut = "";
    
    switch(randomInt){
      case 0: objOut = "flower";
        break;
        
      case 1: objOut = "plant";
        break;
        
      case 2: objOut = "worm";
        break;
        
      case 3: objOut = "beetle";
        break;
        
      case 4: objOut = "fruit";
        break;
        
      case 5: objOut = "ant";
        break;
        
      case 6: objOut = "food";
        break;
        
      case 7: objOut = "treat";
        break;
        
      case 8: objOut = "vegetable";
        break;
        
      case 9: objOut = "legume";
        break;
        
      default: System.out.print("there is an error");
    }
    
    return objOut;
    
  }

  public static String Thesis(){
 Scanner scan = new Scanner (System.in);
    String yesOrNo = "Yes";
    String thesisString = "";
    
    //calls other methods to assign variables with a word (String)
    String adjective = Adjective();
    String adjective2 = Adjective();
    String adjective3 = Adjective();
    String subNoun = SubNoun();
    String verb = Verb();
    String objNoun = ObjNoun();
    
      //generates a random sentence, or more than one if the user desires   
      while(yesOrNo.equals("Yes")){
      thesisString = ("The " + adjective + " and " + adjective2 + " " + subNoun + " " + verb + " the " + adjective3 + " " + objNoun + ".");
      System.out.println(thesisString);
      System.out.println("Would you like another thesis sentence? Yes or No...");
      yesOrNo = scan.next();
        if(yesOrNo.equals("yes") | yesOrNo.equals("Yes")){    //if user says 'yes' the program will generate another sentence, first letter capitlization doesn't matter
          yesOrNo = "Yes";
          adjective = Adjective();
          adjective2 = Adjective();
          adjective3 = Adjective();
          subNoun = SubNoun();
          verb = Verb();
          objNoun = ObjNoun();
          continue;
        }
          
        else if(yesOrNo.equals("No")){      //if user says 'no', the last printed sentence will stand, again punctuation doesn't matter 
          break;
        }
        else if(yesOrNo.equals("no")){
          break;
        }  
         else{
           System.out.println("Please enter 'Yes' or 'No'");
           yesOrNo = scan.next();
         }
        }
  System.out.println("");
  System.out.println(thesisString);
  return subNoun;
} //generates thesis and output the subject noun
 
  public static String SentenceTwo(String thesisNounMain){ //uses subject noun from Thesis for another sentence
    
    String sentenceTwo = "";
    String thesisNoun = thesisNounMain;
    
    Random randy = new Random();
    int randomInt = randy.nextInt(3);
    

        switch (randomInt){
          case 0:
          case 1:
        sentenceTwo = ("This " + thesisNoun + " " + Verb() + " " + Adjective() + " " + ObjNoun() + "s often.");
            break;
          default :
        sentenceTwo = ("It never " + Verb() + " " + Adjective() + " " + ObjNoun() + "s though.");
        break;
    }
    
    return sentenceTwo;
  }
  
  public static String Conclusion(String thesisNounMain){
    
    String thesisNoun = thesisNounMain; //pulls subject noun from main
    String conclusion = "";
      
      conclusion = ("That " + thesisNoun + " " + Verb() + " their " + ObjNoun() + "!");
    
    return conclusion;  
  }
    
  public static void main (String [] args){

      //input variables
      String thesisNounMain = Thesis();   //pulls subject noun from Thesis method
      Random randy = new Random();  
      int randomInt = randy.nextInt(4);
    
      //prints action sentences a random number of times
      for(int i = 0; i <= randomInt; i++){ 
      System.out.println(SentenceTwo(thesisNounMain));
      }
    
      //prints conclusion sentence
      System.out.println(Conclusion(thesisNounMain));
    
    
    
    
    
  }  
}
    
    
    
    
    
    
    
    
    