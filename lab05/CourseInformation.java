   //Program by Ryan Vignogna, for CSE 02 Lab 05 on 10/5/18
   //
   // 
   //
   //
   //
import java.util.Scanner;

public class CourseInformation{
  
  public static void main(String args[]){
    
    //input variables
    Scanner sc = new Scanner (System.in);
    
    int number = 0; //course number
    String name = ""; //department name
    int meet = 0; //times the class meets per week
    int start = 0; //time the class begins
    String prof = ""; //intructor of the class
    int students = 0; //number of students in the class
    
    //course number
    boolean courseNum = false;   
    while (courseNum==false){
      System.out.print("Enter the course number: "); 
      courseNum = sc.hasNextInt();  
    if (courseNum){
      number=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter course number as an integer value");
      sc.next();
      }
    }
      
    //department name
     boolean courseName = false;   
    while (courseName==false){
      System.out.print("Enter the course name: "); 
      courseName = sc.hasNext();  
    if (courseName){
      name=sc.next();
    }
      else{
        System.out.println("Error! Please enter course name as a String");
      sc.next();
      }
    }
        
    //time the course meets per week
     boolean courseMeet = false;   
    while (courseMeet==false){
      System.out.print("Enter the days per week the course meets: "); 
      courseMeet = sc.hasNextInt();  
    if (courseMeet){
      meet=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter meeting number as an integer value");
      sc.next();
      }
    }
    
    //time course begins  
     boolean courseTime = false;   
    while (courseTime==false){
      System.out.print("Enter the time the course begins (in military time, eg 8pm is entered as '1600': "); 
      courseTime = sc.hasNextInt();  
    if (courseTime){
      start=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter course time as an integer");
      sc.next();
      }
    }
    
    //name of instructor
     boolean courseProf = false;   
    while (courseProf==false){
      System.out.print("Enter the course instructor's name: "); 
      courseProf = sc.hasNext();  
    if (courseProf){
      prof=sc.next();
    }
      else{
        System.out.println("Error! Please enter the instructor's name as a String");
      sc.next();
      }
    }
      
    //number of students
     boolean courseStudents = false;   
    while (courseStudents==false){
      System.out.print("Enter the number of students in the course: "); 
      courseStudents = sc.hasNextInt();  
    if (courseStudents){
      students=sc.nextInt();
    }
      else{
        System.out.println("Error! Please enter number of students as an integer value");
      sc.next();
      }
    }
      
    
    
    
    
    
    
    
    
    
    
    
      
      
      
      
    }
 }
    
    
    
    
    
    
    
    
  
 