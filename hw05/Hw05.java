import java.util.Scanner;

 public class Hw05{
   public static void main (String args []){
 
    
    Scanner myScanner = new Scanner (System.in);
    
    int handCount = 0; //number of hands to generate
  
    //ensure integer input
    System.out.println("How many hands of cards do you wish to generate?");
    while(!myScanner.hasNextInt()){
      System.out.println("Incorrect input, please enter an integer");
      myScanner.next();
    }      
    handCount = myScanner.nextInt();
    
    //define card range for random number generator
    int max = 52;
    int min = 1;
    int range = max - min + 1;
    
    //counters
    double pairCounter = 0; //number of pairs total
    double threeCounter = 0; //number of three-of-a-kinds total
    double fourCounter = 0; //number of four-of-a-kinds total
     
    //itertes based on user input for number of hands 
    for(int i = 1; i <= handCount; i++){
       
    //generate cards, ensure none of the five are duplicates using while loops
    int cardNumber1 = (int) (Math.random() * range) + min;
    
    int cardNumber2 = (int) (Math.random() * range) + min;
    while (cardNumber2 == cardNumber1){                     
       cardNumber2 = (int) (Math.random() * range) + min;
    }
    
    int cardNumber3 = (int) (Math.random() * range) + min;
     while ((cardNumber3 == cardNumber1) || (cardNumber3 == cardNumber2)){
       cardNumber3 = (int) (Math.random() * range) + min;
     }
     
    int cardNumber4 = (int) (Math.random() * range) + min;
       while ((cardNumber4 == cardNumber1) || (cardNumber4 == cardNumber2) || (cardNumber4 == cardNumber3)){
       cardNumber4 = (int) (Math.random() * range) + min;
     }
    
    int cardNumber5 = (int) (Math.random() * range) + min;
    while ((cardNumber5 == cardNumber1) || (cardNumber5 == cardNumber2) || (cardNumber5 == cardNumber3) || (cardNumber5 == cardNumber4)){
       cardNumber5 = (int) (Math.random() * range) + min;
    }
     
    //use modulus to assign card values
     int cardMod1 = cardNumber1 % 13;
     int cardMod2 = cardNumber2 % 13;
     int cardMod3 = cardNumber3 % 13;
     int cardMod4 = cardNumber4 % 13;
     int cardMod5 = cardNumber5 % 13;
    
      
      //all single pairs card 1 can make
      if ((cardMod1 == cardMod2) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
       if ((cardMod1 == cardMod3) && (cardMod1 != cardMod2) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
       if ((cardMod1 == cardMod4) && (cardMod1 != cardMod3) && (cardMod1 != cardMod2) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
       if ((cardMod1 == cardMod5) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod2) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }   
      
      //other single pairs card 2 can make
       if ((cardMod2 == cardMod3) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod1) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
       if ((cardMod2 == cardMod4) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod1) && 
          (cardMod2 != cardMod3) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
       if ((cardMod2 == cardMod5) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod1) && 
          (cardMod2 != cardMod3) && (cardMod2 != cardMod4) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
      //other single pairs card 3 can make
       if ((cardMod3 == cardMod4) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod1 != cardMod2) && (cardMod3 != cardMod5) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
       if ((cardMod3 == cardMod5) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod1 != cardMod2) && (cardMod4 != cardMod5)){
        pairCounter++;
      }
      
      //other single pair card 4 can make
       if ((cardMod4 == cardMod5) && (cardMod1 != cardMod3) && (cardMod1 != cardMod4) && (cardMod1 != cardMod5) && (cardMod2 != cardMod3) && 
          (cardMod2 != cardMod4) && (cardMod2 != cardMod5) && (cardMod3 != cardMod4) && (cardMod3 != cardMod5) && (cardMod1 != cardMod2)){
        pairCounter++;
      }

      //four of a kinds
      if ((cardMod1 == cardMod2) && (cardMod1 == cardMod3) && (cardMod1 == cardMod4)){
        fourCounter++;
      }
      
      if ((cardMod1 == cardMod2) && (cardMod1 == cardMod3) && (cardMod1 == cardMod5)){
        fourCounter++;
      }
      
      if ((cardMod1 == cardMod2) && (cardMod1 == cardMod4) && (cardMod1 == cardMod5)){
        fourCounter++;
      }
      
      if ((cardMod1 == cardMod3) && (cardMod1 == cardMod4) && (cardMod1 == cardMod5)){
        fourCounter++;
      }
      
      if ((cardMod2 == cardMod3) && (cardMod2 == cardMod4) && (cardMod2 == cardMod5)){
        fourCounter++;
      }
      
      //three of a kinds
      if ((cardMod1 == cardMod2) && (cardMod1 == cardMod3) && (cardMod1 != cardMod4) && (cardMod1 !=cardMod5)){
        threeCounter++;
      }
      
      if ((cardMod2 == cardMod3) && (cardMod2 == cardMod4) && (cardMod2 != cardMod1) && (cardMod2 !=cardMod5)){
        threeCounter++;
      }
      
       if ((cardMod3 == cardMod4) && (cardMod3 == cardMod5) && (cardMod1 != cardMod3) && (cardMod2 !=cardMod3)){
        threeCounter++;
      }
      
        if ((cardMod4 == cardMod5) && (cardMod4 == cardMod1) && (cardMod4 != cardMod2) && (cardMod4 !=cardMod3)){
        threeCounter++;
      }
      
      if ((cardMod5 == cardMod1) && (cardMod5 == cardMod2) && (cardMod5 != cardMod4) && (cardMod3 !=cardMod5)){
        threeCounter++;
      }
      
       if ((cardMod1 == cardMod3) && (cardMod1 == cardMod4) && (cardMod1 != cardMod2) && (cardMod1 !=cardMod5)){
        threeCounter++;
      }
      
          if ((cardMod1 == cardMod3) && (cardMod1 == cardMod5) && (cardMod1 != cardMod4) && (cardMod1 !=cardMod2)){
        threeCounter++;
      }
      
        if ((cardMod1 == cardMod2) && (cardMod1 == cardMod4) && (cardMod1 != cardMod3) && (cardMod1 !=cardMod5)){
        threeCounter++;
      }
      
      if ((cardMod2 == cardMod4) && (cardMod2 == cardMod5) && (cardMod1 != cardMod2) && (cardMod2 !=cardMod3)){
        threeCounter++;
      }
      
       if ((cardMod2 == cardMod3) && (cardMod2 == cardMod5) && (cardMod2 != cardMod4) && (cardMod2 !=cardMod1)){
        threeCounter++;
      }
      
      
      
      
    } 
    
     //make probabilities have three decimal places
     double x = (double) Math.round((pairCounter/handCount)*1000d)/1000d;
     double y = (double) Math.round((threeCounter/handCount)*1000d)/1000d;
     double z = (double) Math.round((fourCounter/handCount)*10000d)/10000d;
     
     System.out.println("Number of loops run: " + handCount);
     System.out.println("Probability of Pair: " + x);
     System.out.println("Probability of Three of a Kind: " + y);
     System.out.println("Probability of Four of a Kind: " + z);
     
     
   }
 }