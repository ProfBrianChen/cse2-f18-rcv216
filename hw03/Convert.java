//Program by Ryan Vignogna 9/17/18 for CSE 02, HW 03
import java.util.Scanner;
// Program to determine rainfall in a given area, based on inputs of average rainfall and acreage
//
//
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {

         Scanner myScanner = new Scanner (System.in);
          
          //Ask the user for the amount of land affected by a hurricane, store value as acresAffected
          System.out.print("Enter the affected area in acres: ");
          double acresAffected = myScanner.nextDouble ();
          
          //Ask the user for average rainfall, store value as rainFall
          System.out.print("Enter the rainfall in the affected area: ");
          double rainFall = myScanner.nextDouble ();
          
          double rainfallAcres = rainFall * acresAffected; //rain in inches falling on some acreage of land, from inputs
         
          //conversions
          double rainfallGallons = rainfallAcres * 27154.285990761; //roughly 27,154 gallons of rain per 1 inch of rain in 1 acre
          double rainFallCubicMiles = rainfallGallons * 9.08169724455e-13; //1 gallon equals 9.09169e-13 cubic miles
           
          //print the amount of rain in cubic miles
          System.out.println(rainFallCubicMiles + " cubic miles");
        }
}