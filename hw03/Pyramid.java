//Program by Ryan Vignogna 9/17/18 for CSE 02, HW 03
import java.util.Scanner;
// Program to prompt user to input dimmensions of a pyramid and output the volume
//
//
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {

         Scanner myScanner = new Scanner (System.in);
          
          //Ask the user for the square side of the pyramid base, store as squareLength
          System.out.print("The square side of the pyramid is (input length): ");
          double squareLength = myScanner.nextDouble ();
          
          //Ask the user for the height of the pyramid, store as heightLength
          System.out.print("The height of the pyramid is (input length): ");
          double heightLength = myScanner.nextDouble();
          
          //calculate and output the volume of the pyramid, based on equation: volume = (l*l*h)/3 ...this assumes the base is a square
          double pyramidVolume = (Math.pow(squareLength,2) * heightLength) / 3;
          System.out.println("The volume inside the pyramid is: " + pyramidVolume);
          
        }
}