//////////////
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints WelcomeClass and Lehigh ID to screen and biography
    System.out.println("  -----------  "); //prints first line border
    System.out.println("  | WELCOME | "); //prints WELCOME message
    System.out.println("  -----------  "); //prints bottom line border
    System.out.println("  ^  ^  ^  ^  ^  ^ "); //prints top of decoration
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); //prints next line of decoration
    System.out.println("<-R--C--V--2--1--6->"); //prints lehigh id
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / "); //prints second from bottom line of decoration, extra slashes added to avoid error
    System.out.println("  v  v  v  v  v  v "); //bottom of decoration
    System.out.println("My name is Ryan Vignogna. I'm a Biochemistry PhD student in the Department of Biological Sciences at Lehigh,"
                       + " where I study genome evolution in yeast.");
    System.out.println("Outside of lab I enjoy scuba diving, watching soccer, and visiting breweries."); //prints tweet-like biographcial statement
    
  }
  
}