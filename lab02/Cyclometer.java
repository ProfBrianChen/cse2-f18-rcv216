   //Program by Ryan Vignogna, for CSE 02 Lab 02 on 9/7/18
   /*
   prints bicycle trip data; including trip duration,
   tire rotations per trip (counts), trip distance, and total distance
   of two different trips...all to keep a good record of trip data given from
   a bicycle cyclometer
   */
public class Cyclometer{
  //main method for any Java program
  public static void main(String args[]){
    
    
 //our input data
    int secsTrip1 = 480;    //integer defined and assigned, duration of first trip
    int secsTrip2 = 3220;   //integer defined and assigned, duration of second trip
    int countsTrip1 = 1561; //integer defined and assigned, wheel rotations in trip 1
    int countsTrip2 = 9037; //integer defined and assigned, wheel rotations in trip 2
    
 //our intermediate variables and output data, note that commas mean the 'double' designation is carried to next lines
    double wheelDiameter = 27.0, //diameter of wheel for distance calculation
    PI = 3.14159, //defining pi to five decimal places
    feetPerMile = 5280, //5280 feet per 1 mile
    inchesPerFoot = 12, //12 inches per 1 foot
    secondsPerMinute = 60; //60 seconds per one minute
    double distanceTrip1, distanceTrip2, totalDistance; //variables of output distance traveled data
  
 //Print the stored variables
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had "
                       + countsTrip1 +" counts.");  //prints trip 1 duration and cyclo counts
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had "
                       + countsTrip2 +" counts."); //prints trip 2 duration and cyclo counts
 
 //run the calculations, store the values
 //calculate trip length as a function of wheel rotations 
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    //the above gives distance in inchesPerFoot of trip 1
    //for each count, a wheel rotation travels the diameter times PI aka the circumference
    distanceTrip1 /= inchesPerFoot * feetPerMile; //gives distance in miles of trip 1
    distanceTrip2 = countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile; //distance of trip 2 in miles
    totalDistance = distanceTrip1 + distanceTrip2; //total distance of trip 1 plus trip 2
    
 //Print the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); //prints trip1 in miles
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles"); //prints trip2 in miles
	  System.out.println("The total distance was " + totalDistance + " miles"); //prints total of trip 1 and 2 in miles

  } //end of main method
} //end of class