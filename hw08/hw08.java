//Program for Lab08 by Ryan Vignogna on 13 November 2018
//
//Prints a deck of cards, shuffles the cards randomly, then prints out hands
//
//
//
import java.util.*;

public class hw08{
  
public static String printArray(String[] list){
  
  int listSize = list.length;
  
  for(int i = 0; i < listSize; i++){
    System.out.print(list[i] + " ");
  }
  return " ";
}  
  
public static String[] shuffle(String[] list){
  
  //String[] shuffleArray = list;
  Random randy = new Random();
  int shuffleNum = ((randy.nextInt(200)+1)+51); //number of times the shuffle will occur, between 52 and 252 times
  int randIndex = 0; //the index to swap places with index 0
  String temp = " ";
  
    //shuffles cards
    for (int j = list.length - 1; j >= 0; j--){
    randIndex = (randy.nextInt(51)+1);
    temp = list[randIndex];
    list[randIndex] = list[j];
    list[j] = temp;
  }
  return list;
} 
  
public static String[] getHand(String[] list, int index, int numCards){
  
  String[] hand = new String [numCards];
  
  for (int i = index; i> index - numCards; i--){
    hand[index - i] = list[i];
  }
  
  return hand;
  
}
  
public static void main(String[] args) { 
  
Scanner scan = new Scanner(System.in); 
  
//suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] cardsBackup = new String[52];
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
  
//populates array with cards in order  
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
  
  for (int j=0; j<52; j++){
    cardsBackup[j]=cards[j];
  }
  
//passes cards array into Shuffle method  
System.out.println();
System.out.println("Shuffled:");
shuffle(cards); 
printArray(cards);
System.out.println("");
  
//generates hand of size numCards  
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   System.out.println("Hand:"); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt();
  
  //creates a new deck and shuffles it if the hand size is greater than remaining cards in the deck
    if (numCards>index){
     for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
     }
     System.out.println("New shuffled deck:");
     shuffle(cards); 
     printArray(cards);
     System.out.println("");
     index = 51;
      }  
    }
  
  } 
}
